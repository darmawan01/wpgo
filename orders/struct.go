package orders

import "time"

type orders struct {
	ID        int       `json:"id"`
	Date      time.Time `json:"date"`
	Status    string    `json:"status"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Address   string    `json:"address"`
	Phone     string    `json:"phone"`
	Price     int       `json:"price"`
}

type ordersMeta struct {
	Data    []orders `json:"orders"`
	Page    int      `json:"page"`
	Limit   int      `json:"limit"`
	Total   int      `json:"total"`
	OrderBy string   `json:"order_by"`

	// Unexposed field
	params map[string]string
}
