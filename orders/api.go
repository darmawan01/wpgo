package orders

import (
	"database/sql"
	"log"
	"net/http"
	"strconv"
	"wpgo/utils"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

type OrderAPI struct {
	Router *gin.RouterGroup
	DB     *sql.DB
	Redis  *redis.Client
}

func (api *OrderAPI) Register() {
	api.Router.GET("/orders", api.ordersListRequest)

	log.Println("Orders API Registered")
}

func (api *OrderAPI) ordersListRequest(c *gin.Context) {
	var meta ordersMeta
	var err error
	params := make(map[string]string, 0)

	if meta.Limit, err = strconv.Atoi(utils.Sanitize(c.DefaultQuery("per_page", "10"))); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "invalid-limit-type"})
		return
	}
	if meta.Page, err = strconv.Atoi(utils.Sanitize(c.DefaultQuery("page", "1"))); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "invalid-page-type"})
		return
	}
	if !utils.IsEmpty(c.Query("status")) {
		params["wc_status"] = utils.Sanitize(c.Query("status"))
	}
	if !utils.IsEmpty(c.Query("email")) {
		params["email"] = utils.Sanitize(c.Query("email"))
	}

	meta.params = params

	result, err := api.all(meta)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, result)
}
