package db

import (
	"log"
	"os"
	"strconv"
	"wpgo/utils"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

type dbConfig struct {
	// DB
	Host     string
	Port     int
	User     string
	Password string
	DbName   string

	// Redis
	RedisHost string
	RedisPort string
}

func getConfig() dbConfig {
	if utils.IsEqual(gin.Mode(), gin.ReleaseMode) {
		port, err := strconv.Atoi(os.Getenv("DB_PORT"))
		if err != nil {
			panic(err)
		}

		return dbConfig{
			Host:      os.Getenv("DB_HOST"),
			Port:      port,
			User:      os.Getenv("DB_USER"),
			Password:  os.Getenv("DB_PASS"),
			DbName:    os.Getenv("DB_NAME"),
			RedisHost: os.Getenv("REDIS_HOST"),
			RedisPort: os.Getenv("REDIS_PORT"),
		}
	}

	mode := "local"
	confPath := "./configs/config.json"
	if utils.IsEqual(gin.Mode(), gin.TestMode) {
		mode = "testing"
		confPath = "../configs/config.json"
	}

	viper.SetConfigFile(confPath)
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}

	c := viper.Sub(mode)
	return dbConfig{
		Port:      c.GetInt("DB_PORT"),
		Host:      c.GetString("DB_HOST"),
		User:      c.GetString("DB_USER"),
		Password:  c.GetString("DB_PASS"),
		DbName:    c.GetString("DB_NAME"),
		RedisHost: os.Getenv("REDIS_HOST"),
		RedisPort: os.Getenv("REDIS_PORT"),
	}

}
