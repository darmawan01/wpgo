package vendors

import (
	"database/sql"
	"fmt"
	"log"
)

func (api *VendorAPI) all(meta vendorsMeta) (results vendorsMeta, err error) {
	query := `
		SELECT
			wt.term_id as id,
			wt.name as name,
			hotel_id.meta_value as hotel_id,
			telegram_bot_chat_id.meta_value as telegram_bot_chat_id
		FROM
			wp_terms wt
			INNER JOIN wp_term_taxonomy wtt 
				ON wt.term_id = wtt.term_id 
				AND wtt.taxonomy = 'wcpv_product_vendors'
			
			LEFT JOIN wp_termmeta hotel_id
				ON wt.term_id = hotel_id.term_id
				AND hotel_id.meta_key = 'hotel_id'
				
			LEFT JOIN wp_termmeta telegram_bot_chat_id
				ON wt.term_id = telegram_bot_chat_id.term_id
				AND telegram_bot_chat_id.meta_key = 'telegram_bot_chat_id'
	`

	if err = api.DB.QueryRow(
		fmt.Sprintf("SELECT COUNT(*) FROM (%s) AS count", query),
	).Scan(&meta.Total); err != nil && err != sql.ErrNoRows {
		log.Println("Query(): ", err.Error())
		return
	}

	query += fmt.Sprintf("LIMIT %d OFFSET %d", meta.Limit, (meta.Page-1)*meta.Limit)

	var rows *sql.Rows
	rows, err = api.DB.Query(query)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Query(): ", err.Error())
		return
	}
	defer rows.Close()

	data := make([]vendors, 0)
	for rows.Next() {
		var vendor vendors
		var ID, hotelID sql.NullInt64
		var name, telegramBotChatID sql.NullString

		if err = rows.Scan(
			&ID,
			&name,
			&hotelID,
			&telegramBotChatID,
		); err != nil {
			log.Println("Scan(): ", err.Error())
			return
		}
		vendor.ID = int(ID.Int64)
		vendor.Name = name.String
		vendor.HotelID = int(hotelID.Int64)
		vendor.TelegramBotChatID = telegramBotChatID.String

		data = append(data, vendor)
	}
	meta.Data = data
	results = meta
	return
}

func (api *VendorAPI) get() (vendor vendors, err error) {
	return
}

func (api *VendorAPI) add() (err error) {
	return
}

func (api *VendorAPI) update() (err error) {
	return
}

func (api *VendorAPI) delete() (err error) {
	return
}
