package vendors

type vendors struct {
	ID                int    `json:"id"`
	Name              string `json:"mame"`
	HotelID           int    `json:"hotel_id"`
	TelegramBotChatID string `json:"telegram_bot_chat_id"`
}

type vendorsMeta struct {
	Data    []vendors `json:"vendors"`
	Page    int       `json:"page"`
	Limit   int       `json:"limit"`
	Total   int       `json:"total"`
	OrderBy string    `json:"order_by"`

	// Unexposed field
	params map[string]string
}
