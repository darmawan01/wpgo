package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"wpgo/auth"
	"wpgo/db"
	"wpgo/orders"
	"wpgo/posts"
	"wpgo/products"
	"wpgo/utils"
	"wpgo/vendors"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

type appConfig struct {
	port string
}

var config *appConfig
var dbConn *sql.DB
var redisConn *redis.Client

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	config = getAppConfig()
	dbConn = db.InitMysql()
	redisConn = db.InitRedis()
}

func main() {

	router := gin.Default()
	router.Use(gin.Recovery())

	fmt.Println("Configurations:")
	fmt.Println("App Port: ", config.port)
	fmt.Println("----------------------------")

	/*
		Registering module below, Don't Forgot to add the module also at build script.
		See README.md for detail.
	*/
	routGroup := router.Group("/api/v1")

	productAPI := products.ProductAPI{
		Router: routGroup,
		DB:     dbConn,
		Redis:  redisConn,
	}
	productAPI.Register()

	vendorAPI := vendors.VendorAPI{
		Router: routGroup,
		DB:     dbConn,
		Redis:  redisConn,
	}
	vendorAPI.Register()

	orderAPI := orders.OrderAPI{
		Router: routGroup,
		DB:     dbConn,
		Redis:  redisConn,
	}
	orderAPI.Register()

	authAPI := auth.AuthAPI{
		Router: routGroup,
		DB:     dbConn,
		Redis:  redisConn,
	}
	authAPI.Register()

	postsAPI := posts.PostsAPI{
		Router: routGroup,
		DB:     dbConn,
		Redis:  redisConn,
	}
	postsAPI.Register()

	// End of registered module

	router.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS"},
		AllowHeaders: []string{
			"X-Requested-With",
			"Content-Type",
			"Authorization",
			"Cache-Control",
		},
		ExposeHeaders: []string{"Authorization"},
	}))
	log.Fatal(router.Run(":" + config.port))
}

func getAppConfig() (c *appConfig) {
	appPort := "8080"
	if !utils.IsEmpty(os.Getenv("PORT")) {
		appPort = os.Getenv("PORT")
	}

	c = &appConfig{
		port: appPort,
	}
	return
}
