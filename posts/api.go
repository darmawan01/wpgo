package posts

import (
	"database/sql"
	"log"
	"wpgo/utils"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

type PostsAPI struct {
	Router *gin.RouterGroup
	DB     *sql.DB
	Redis  *redis.Client
}

func (api *PostsAPI) Register() {
	api.Router.Use(utils.Middleware())
	{
		api.Router.POST("/posts", api.postsListRequest)
	}
	log.Println("Posts API Registered")
}

func (api *PostsAPI) postsListRequest(c *gin.Context) {

}
