package products

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"wpgo/db"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var api *ProductAPI
var resp = httptest.NewRecorder()
var req *gin.Context
var route *gin.Engine

func TestInit(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	req, route = gin.CreateTestContext(resp)

	api = &ProductAPI{
		DB:    db.InitMysql(),
		Redis: db.InitRedis(),
	}
}

func TestProductsListRequest(t *testing.T) {
	route.GET("/products", api.productsListRequest)

	req.Request, _ = http.NewRequest(http.MethodGet, "/products", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

	var data map[string]interface{}

	res, _ := ioutil.ReadAll(resp.Body)
	err := json.Unmarshal(res, &data)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, float64(10), float64(len(data["products"].([]interface{}))), "Expected got 10 products")

	// Test per page
	req.Request, _ = http.NewRequest(http.MethodGet, "/products?per_page=20&page=1", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

	res, _ = ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(res, &data)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, float64(20), float64(len(data["products"].([]interface{}))), "Expected got 20 products")

	// Test by category_id
	req.Request, _ = http.NewRequest(http.MethodGet, "/products?per_page=10&page=1&category_id=16", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

	res, _ = ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(res, &data)
	assert.Equal(t, nil, err, "Expected no error")
	resulCategoryID := data["products"].([]interface{})[0].(map[string]interface{})["category_id"]
	assert.Equal(t, float64(16), resulCategoryID, "Expected got 16 category id")

	// Test by vendor_id
	req.Request, _ = http.NewRequest(http.MethodGet, "/products?per_page=10&page=1&vendor_id=19", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

	res, _ = ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(res, &data)
	assert.Equal(t, nil, err, "Expected no error")

	resulVendorID := data["products"].([]interface{})[0].(map[string]interface{})["vendor_id"]
	assert.Equal(t, float64(19), resulVendorID, "Expected got 27 category id")

	// Test search
	req.Request, _ = http.NewRequest(http.MethodGet, "/products?per_page=10&page=1&search=bayam", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

	res, _ = ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(res, &data)
	assert.Equal(t, nil, err, "Expected no error")

	assert.Equal(t, true, len(data["products"].([]interface{})) > 0, "Expected got some result with keyword")
}
