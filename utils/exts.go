package utils

import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gopkg.in/go-playground/validator.v9"
)

// RespondwithJSON func
func RespondwithJSON(w http.ResponseWriter, code int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	response, err := json.Marshal(payload)
	if err != nil {
		log.Fatal("Can't parse into json with err: ", err.Error())
	}
	w.WriteHeader(code)
	w.Write(response)
}

// DataFormat func
func DataFormat(msg string, data interface{}) interface{} {
	result := map[string]interface{}{
		"msg":  msg,
		"data": data,
	}
	return result
}

// ErrFormat func
func ErrFormat(msg string, data interface{}) interface{} {
	result := map[string]interface{}{
		"msg":  msg,
		"data": nil,
	}
	return result
}

// GetIDParam func
func GetIDParam(r *http.Request) string {
	id := mux.Vars(r)["id"]
	return id
}

// GetQueryParam func
func GetQueryParam(r *http.Request, key ...string) map[string]string {
	queryQV := make(map[string]string)
	for _, q := range key {
		query := r.URL.Query().Get(q)
		queryQV[q] = query
	}
	return queryQV
}

// IsEmpty func
func IsEmpty(str string) bool {
	if len(str) <= 0 || str == "" {
		return true
	}
	return false
}

// IsEqual func
func IsEqual(str1 string, str2 string) bool {
	if strings.Compare(str1, str2) == 0 {
		return true
	}
	return false
}

// ValidateStruct func
func ValidateStruct(model interface{}) (bool, error) {
	validate := validator.New()
	if err := validate.Struct(model); err != nil {
		return false, err
	}
	return true, nil
}

// StrToBool func
func StrToBool(str string) (ok bool) {
	var err error
	if ok, err = strconv.ParseBool(str); err != nil {
		ok = false
		return
	}
	return
}

// Sanitize func
func Sanitize(str string) string {
	sanitize, _ := regexp.Compile(`['"]`)
	return sanitize.ReplaceAllString(str, "")
}
